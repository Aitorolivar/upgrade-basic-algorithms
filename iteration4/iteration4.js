// 1.1 Consigue el valor "HULK" del array de cars y muestralo por consola.
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
// console.log(avengers[0]);


// 1.2 Cambia el primer elemento de avengers a "IRONMAN"
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

avengers[0] = "IRONMAN";
// console.log(avengers);


// 1.3 Alert numero de elementos en el array usando la propiedad correcta de Array.
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

// console.log(avengers.length);


// 1.4 Añade 2 elementos al array: "Morty" y "Summer". 
// Muestra en consola el último personaje del array
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];


const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
rickAndMortyCharacters.push("Morty", "Summer");
// console.log(rickAndMortyCharacters);
// console.log(rickAndMortyCharacters[rickAndMortyCharacters.length-1]) Forma correcta de acceder a la ultima posición.
// console.log(rickAndMortyCharacters[4]); Si añadimos mas elementos al array no es valido por que ya no seria el ultimo.

// 1.5 Elimina el último elemento del array y muestra el primero y el último por consola.
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"]

const rickAndMortyCharacters2 = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
rickAndMortyCharacters2.pop();
// console.log(rickAndMortyCharacters);
let elementFirstAndLast = rickAndMortyCharacters2[0] + ' ' + rickAndMortyCharacters2[4];
// console.log(elementFirstAndLast);


// 1.6 Elimina el segundo elemento del array y muestra el array por consola.
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];

const rickAndMortyCharacters3 = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
rickAndMortyCharacters3.splice(1,1);//Elimina indicando la posición.
// console.log(rickAndMortyCharacters3);
let newArray = rickAndMortyCharacters3.slice(1,5);//Crea una copia de una parte del array indicando la posición. 
// console.log(newArray);

